﻿(function () {
    "use strict";

    var module = angular.module("ogApp", ["common.services"]);

    module.value("ogApp");

}());
﻿(function () {
    "use strict";

    var module = angular.module("ogApp");

    function controller($http, appSettings) {

        var model = this;

        var value1 = '';
        var value2 = '';
        var operation;

        model.output = '';
        model.appendNumber = function (num) {

            if (num === '.' && model.output.includes('.')) return;

            if (operation) {
                value2 += num;
                model.output = value2;
            } else {
                value1 += num;
                model.output += num;
            }
        };

        model.op = function (op) {
            if (operation) {
                model.compute();
            }
            if (op === '+') operation = 'add'
            else if (op === '-') operation = 'subtract'
            else if (op === '/') operation = 'divide'
            else if (op === '*') operation = 'multiply'
        };

        model.clear = function () {
            value1 = '';
            value2 = '';
            model.output = '';
        };

        model.compute = function () {
            if (operation) {

                var url = appSettings.serverPath + "api/calc?value1=" + 1 * value1 + "&value2=" + 1 * value2 + "&operation=" + operation;

                return $http.get(url)
                    .then(function (response) {
                        value1 = response.data.result;
                        value2 = '';
                        model.output = value1;
                        operation = undefined;
                    })
            }
        };
    }

    module.component("ogCalculator", {
        templateUrl: "app/calculator/calculator.component.html",
        controllerAs: "model",
        controller: ["$http", "appSettings", controller]
    });

}());
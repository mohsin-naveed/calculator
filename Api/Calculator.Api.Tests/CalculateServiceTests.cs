﻿using Calculator.Api.Core;
using System;
using Xunit;

namespace Calculator.Api.Tests
{
    public class CalculateServiceTests
    {
        [Fact]
        public void Compute_AddTwoValues_WhenCalledForAddOperation()
        {
            var calculatorService = new CalculatorService();

            var result = calculatorService.Compute(2, 2, "add");

            Assert.Equal(4, result);
        }

        [Fact]
        public void Compute_Subtract_WhenCalledForSubtractOperation()
        {
            var calculatorService = new CalculatorService();

            var result = calculatorService.Compute(10, 2, "subtract");

            Assert.Equal(8, result);
        }

        [Fact]
        public void Compute_MultiplyTwoValues_WhenCalledForMultiplyOperation()
        {
            var calculatorService = new CalculatorService();

            var result = calculatorService.Compute(12, 2, "multiply");

            Assert.Equal(24, result);
        }

        [Fact]
        public void Compute_Divide_WhenCalledForDivideOperation()
        {
            var calculatorService = new CalculatorService();

            var result = calculatorService.Compute(4, 2, "divide");

            Assert.Equal(2, result);
        }

        [Fact]
        public void Compute_ThrowExceptionForInvalidOperation()
        {
            var calculatorService = new CalculatorService();

            Assert.Throws<InvalidOperationException>(() => calculatorService.Compute(4, 2, "invalid"));
        }

    }
}

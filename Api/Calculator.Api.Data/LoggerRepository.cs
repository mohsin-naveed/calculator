﻿using Calculator.Api.Domain;
using Calculator.Api.Domain.Interfaces;
using System;

namespace Calculator.Api.Data
{
    public class LoggerRepository : ILoggerRepository
    {
        private readonly CalculatorContext _context;

        public LoggerRepository(CalculatorContext context)
        {
            _context = context;
        }
        public void Save(string ipAddress)
        {
            var log = new Log
            {
                IpAddress = ipAddress,
                CreatedOn = DateTime.Now
            };

            _context.Logs.Add(log);
            _context.SaveChanges();
        }
    }
}
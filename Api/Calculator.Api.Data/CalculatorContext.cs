﻿using Calculator.Api.Domain;
using System.Data.Entity;

namespace Calculator.Api.Data
{
    public class CalculatorContext : DbContext
    {
        public CalculatorContext() : base("CalculatorConnectionString")
        {
            Database.SetInitializer<CalculatorContext>(new CreateDatabaseIfNotExists<CalculatorContext>());
        }

        public DbSet<Log> Logs { get; set; }
    }
}

﻿using Autofac;
using Autofac.Integration.WebApi;
using Calculator.Api.Core;
using Calculator.Api.Data;
using Calculator.Api.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace Calculator.Api.App_Start
{
    public class AutofacConfig
    {
        public static void Register()
        {
            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            RegisterServices(builder);
            builder.RegisterWebApiFilterProvider(config);
            builder.RegisterWebApiModelBinderProvider();
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static void RegisterServices(ContainerBuilder bldr)
        {
            bldr.RegisterType<Logger>()
             .As<ILogger>()
             .InstancePerRequest();

            bldr.RegisterType<CalculatorContext>()
        .InstancePerRequest();

            bldr.RegisterType<LoggerRepository>()
              .As<ILoggerRepository>()
              .InstancePerRequest();

            bldr.RegisterType<CalculatorService>()
             .As<ICalculatorService>()
             .InstancePerRequest();
        }
    }
}
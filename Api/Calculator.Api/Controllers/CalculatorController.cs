﻿using Calculator.Api.Domain.Interfaces;
using System;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Calculator.Api.Controllers
{
    [EnableCors("https://localhost:44307", "*", "*")]
    [RoutePrefix("api/calc")]
    public class CalculatorController : ApiController
    {
        private readonly ICalculatorService _calculatorService;
        private readonly ILogger _logger;

        public CalculatorController(ICalculatorService calculatorService, ILogger logger)
        {
            _calculatorService = calculatorService;
            _logger = logger;
        }

        [Route]
        public IHttpActionResult Get(double value1, double value2, string operation)
        {
            try
            {
                _logger.Log(HttpContext.Current.Request.UserHostAddress);

                var result = _calculatorService.Compute(value1, value2, operation);

                return Ok(new { Result = result });
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}

﻿using Calculator.Api.Domain.Interfaces;
using System;

namespace Calculator.Api.Core
{
    public class CalculatorService : ICalculatorService
    {
        public CalculatorService() {}
        public double Compute(double value1, double value2, string operation)
        {
            switch (operation.ToUpper())
            {
                case "ADD":
                    return Add(value1, value2);
                case "SUBTRACT":
                    return Subtract(value1, value2);
                case "MULTIPLY":
                    return Multiply(value1, value2);
                case "DIVIDE":
                    return Divide(value1, value2);
                default:
                    throw new InvalidOperationException($"Invalid operation: {operation}");
            }
        }
        private double Add(double value1, double value2)
        {
            return value1 + value2;
        }
        private double Subtract(double value1, double value2)
        {
            return value1 - value2;
        }
        private double Multiply(double value1, double value2)
        {
            return value1 * value2;
        }
        private double Divide(double value1, double value2)
        {
            try
            {
                return value1 / value2;
            }
            catch (DivideByZeroException)
            {
                throw new DivideByZeroException("DIVIDE BY ZERO");
            }
        }
    }
}

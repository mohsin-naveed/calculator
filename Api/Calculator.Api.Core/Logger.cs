﻿using Calculator.Api.Domain.Interfaces;

namespace Calculator.Api.Core
{
    public class Logger : ILogger
    {
        private readonly ILoggerRepository _loggerRepository;

        public Logger(ILoggerRepository loggerRepository)
        {
            _loggerRepository = loggerRepository;
        }
        public void Log(string ipAddress)
        {
            _loggerRepository.Save(ipAddress);
        }
    }
}
﻿using System;

namespace Calculator.Api.Domain
{
    public class Log
    {
        public int Id { get; set; }
        public string IpAddress { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}

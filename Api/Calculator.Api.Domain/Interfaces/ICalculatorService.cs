﻿namespace Calculator.Api.Domain.Interfaces
{
    /// <summary>
    /// Perform mathematical operation for calculator
    /// </summary>
    /// <param name="value1">value1</param>
    /// <param name="value2">value2</param>
    /// <param name="operation">Mathematical operations (add, subtract, multiply, dividd)</param>
    /// <returns></returns>
    public interface ICalculatorService
    {
        double Compute(double value1, double value2, string operation);
    }
}
﻿namespace Calculator.Api.Domain.Interfaces
{
    public interface ILoggerRepository
    {
        void Save(string ipAddress);
    }
}

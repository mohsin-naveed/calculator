﻿namespace Calculator.Api.Domain.Interfaces
{
    public interface ILogger
    {
        void Log(string ipAddress);
    }
}
